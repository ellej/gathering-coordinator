/**
 * Validate a URL string.
 *
 * @param {string} url - The URL to be validated
 * @returns {string | null} The valid URL, otherwise null
 */
export const validateUrl = (url) => {
  try {
    return new URL(url).href
  } catch (err) {
    return null
  }
}
