/**
 * Remove a trailing character from a string.
 *
 * @param {string} str - The string to remove the char from
 * @param {string} char - The character to remove
 * @returns {string} The new string without the trailing character, or same string if no such character
 */
export const removeTrailingChar = (str, char) => (str[str.length - 1] === char) ? str.slice(0, -1) : str

/**
 * Remove a leading character from a string.
 *
 * @param {string} str - The string to remove the char from
 * @param {string} char - The character to remove
 * @returns {string} The new string without the leading character, or same string if no such character
 */
export const removeLeadingChar = (str, char) => (str[0] === char) ? str.slice(1) : str

/**
 * Get the first item containing a search term (case-insensitive).
 *
 * @param {string[]} list - The list of items to search from
 * @param {string} searchTerm - The term to search for
 * @returns {string | undefined} The item if it exists, otherwise undefined
 */
export const search = (list, searchTerm) => {
  // Do case-insensitive search
  const regex = new RegExp(searchTerm, 'i')

  return list.find(item => regex.test(item))
}
