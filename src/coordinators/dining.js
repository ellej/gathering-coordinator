import Day from '../models/day.js'
import FormScraper from '../scrapers/form.js'
import HTTPService from '../services/http.js'
import Reservation from '../models/reservation.js'
import Time from '../models/time.js'
import { removeTrailingChar } from '../utils/strings.js'

/**
 * Coordinator based on restaurant table availability.
 */
class DiningCoordinator {
  /**
   * Create a Dining Coordinator.
   *
   * @param {string} url - The restaurant URL starting point
   */
  constructor (url) {
    this._url = url
  }

  /**
   * Coordinate available table reservations.
   *
   * @param {object[]} daysAndTimes - The days and times to check for reservations ({ day: < Day >, startTime: < Time > })
   * @returns {Reservation[]} Available reservations
   */
  async run (daysAndTimes) {
    const res = await this._login()
    const reservationsUrl = this._getReservationsUrl(res)
    const reqOptions = { headers: { Cookie: this._getCookie(res) } }
    const reservationsHtml = await new HTTPService().getAsText(reservationsUrl, reqOptions)
    const reservationInputs = new FormScraper(reservationsHtml).getInputs('radio')

    return this._getAvailableReservations(daysAndTimes, this._parse(reservationInputs))
  }

  /**
   * Reserve a table at the restaurant.
   *
   * @param {Reservation} reservation - The reservation to make.
   */
  async makeReservation (reservation) {
    const loginRes = await this._login()
    const reservationsUrl = this._getReservationsUrl(loginRes)
    const reqOptions = { headers: { Cookie: this._getCookie(loginRes) } }
    const data = {
      [reservation.idName]: reservation.idValue,
      csrf_token: 'Jishgeny6753ydiayYHSjay0918'
    }
    const reservationRes = await new HTTPService().post(reservationsUrl, data, reqOptions)
    if (!this._isSuccessfulResponse(reservationRes)) {
      throw new Error('Reservation failed. Could not book a table.')
    }
  }

  /**
   * Get available table reservations for specific times of specific days.
   *
   * @param {object[]} daysAndTimes - The days and times to check for reservations ({ day: < Day >, startTime: < Time > })
   * @param {Reservation[]} options - The options to choose from
   * @returns {Reservation[]} Available reservations
   */
  _getAvailableReservations (daysAndTimes, options) {
    const availableReservations = new Set()
    for (const occasion of daysAndTimes) {
      const reservation = this._checkAvailability(occasion.day, occasion.startTime, options)
      if (reservation) {
        availableReservations.add(reservation)
      }
    }

    return [...availableReservations]
  }

  /**
   * Get the available reservation if it exists.
   *
   * @param {Day} day - The day to check for a reservation
   * @param {Time} startTime - The start time to check for a reservation
   * @param {Reservation[]} options - The options to choose from
   * @returns {Reservation | undefined} The reservation if available, otherwise undefined
   */
  _checkAvailability (day, startTime, options) {
    return options.find(option => option.day.equals(day) && option.startTime.equals(startTime))
  }

  /**
   * Get the parsed reservations.
   *
   * @param {object[]} reservationInputs - The inputs with name and value
   * @returns {Reservation[]} Reservations
   */
  _parse (reservationInputs) {
    return reservationInputs.map(input => new Reservation(
      this._valueToDay(input.value),
      this._valueToStartTime(input.value),
      this._valueToEndTime(input.value),
      input.name,
      input.value
    ))
  }

  /**
   * Convert the value representation of the reservation to its corresponding day.
   *
   * @param {string} value - The value representation of the reservation (< firstThreeLettersOfDay >< startTime >< endTime >)
   * @returns {Day} The day of the reservation
   */
  _valueToDay (value) {
    const DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    const firstThreeLettersOfDay = value.substr(0, 3).toLowerCase()

    return new Day(DAYS.find(day => day.startsWith(firstThreeLettersOfDay)))
  }

  /**
   * Convert the value representation of the reservation to its start time.
   *
   * @param {string} value - The value representation of the reservation (< firstThreeLettersOfDay >< startTime >< endTime >)
   * @returns {Time} The time the reservation starts
   */
  _valueToStartTime (value) {
    const INDEX_START_TIME = 3
    const NUMBERS_INCLUDED = 2

    return new Time(value.substr(INDEX_START_TIME, NUMBERS_INCLUDED))
  }

  /**
   * Convert the value representation of the reservation to its end time.
   *
   * @param {string} value - The value representation of the reservation (< firstThreeLettersOfDay >< startTime >< endTime >)
   * @returns {Time} The time the reservation ends
   */
  _valueToEndTime (value) {
    const INDEX_END_TIME = 5
    const NUMBERS_INCLUDED = 2

    return new Time(value.substr(INDEX_END_TIME, NUMBERS_INCLUDED))
  }

  /**
   * Login to the restaurant's reservations area.
   *
   * @returns {Response} The HTTP response
   */
  async _login () {
    const url = removeTrailingChar(this._url, '/') + '/login'
    // login credentials are NOT a secret
    const data = { username: 'zeke', password: 'coys', submit: 'login' }
    const options = { redirect: 'manual' }
    const res = await new HTTPService().post(url, data, options)
    if (!this._isSuccessfulResponse(res)) {
      throw new Error('Incorrect login credentials for restaurant.')
    }

    return res
  }

  /**
   * Check if the login is successful.
   *
   * @param {Response} res The HTTP response
   * @returns {boolean} Whether or not the login is successful
   */
  _isSuccessfulResponse (res) {
    return res.status === 302 || (res.status >= 200 && res.status < 300)
  }

  /**
   * Get the cookie from an HTTP response.
   *
   * @param {Response} res The HTTP response
   * @returns {string} The cookie
   */
  _getCookie (res) {
    return res.headers.get('set-cookie')
  }

  /**
   * Get the URL to the reservation time slots.
   *
   * @param {Response} res The HTTP response
   * @returns {string} The reservations URL
   */
  _getReservationsUrl (res) {
    return res.headers.get('location')
  }
}

export default DiningCoordinator
