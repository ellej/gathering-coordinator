import Day from '../models/day.js'
import HTTPService from '../services/http.js'
import FormScraper from '../scrapers/form.js'
import MovieShowing from '../models/movieShowing.js'
import Time from '../models/time.js'

/**
 * Coordinator based on cinema movie availability.
 */
class MovieCoordinator {
  /**
   * Create a Movie Coordinator.
   *
   * @param {string} url - The cinema URL starting point
   */
  constructor (url) {
    this._url = url
  }

  /**
   * Coordinate available movies.
   *
   * @param {Day[]} days - The days to coordinate movies
   * @returns {MovieShowing[]} Available showings
   */
  async run (days) {
    const cinemaHtml = await new HTTPService().getAsText(this._url)
    const formScraper = new FormScraper(cinemaHtml)
    const allDayOptions = formScraper.getOptions('day')
    const allMovieOptions = formScraper.getOptions('movie')

    return this._getAvailableShowings(days, allDayOptions, allMovieOptions)
  }

  /**
   * An option.
   *
   * @typedef {object} Option
   * @property {string} value - The value representation of the option
   * @property {string} textContent - The name representation of the option
   */

  /**
   * Get all the showings available on the given days.
   *
   * @param {Day[]} days - The days to get available showings for
   * @param {Option[]} allDayOptions - All the day options ({ value: < value >, textContent: < name > })
   * @param {Option[]} allMovieOptions - All the movie options ({ value: < value >, textContent: < name > })
   * @returns {MovieShowing[]} Available showings
   */
  async _getAvailableShowings (days, allDayOptions, allMovieOptions) {
    const availableShowings = []
    // for each of the available days, get all the showings for each movie
    for (const day of days) {
      for (const movieOption of allMovieOptions) {
        const dayValue = this._nameToValue(day.name, allDayOptions)
        const showings = await this._getShowings(dayValue, movieOption.value)
        // store all the showings that are available
        for (const showing of showings) {
          if (this._isAvailable(showing)) {
            availableShowings.push(new MovieShowing(
              movieOption.textContent,
              day,
              new Time(showing.time)
            ))
          }
        }
      }
    }

    return availableShowings
  }

  /**
   * Get all the showings of a movie on a specific day.
   *
   * @param {string} dayValue - The value representation of the day
   * @param {string} movieValue - The value representation of the movie
   * @returns {Promise<object[]>} Promise that resolves with the showings with status, day, time, and movie
   */
  _getShowings (dayValue, movieValue) {
    const url = `${this._url}/check?day=${dayValue}&movie=${movieValue}`

    return new HTTPService().getAsJson(url)
  }

  /**
   * Check whether or not a showing is available.
   *
   * @param {object} showing - The showing object with status property
   * @returns {boolean} Whether or not the showing is available
   */
  _isAvailable (showing) {
    return !!showing.status
  }

  /**
   * Convert the name representation of an option to its value representation.
   *
   * @param {string} name - The name representation of the option
   * @param {Option[]} options - All the options ({ value: < value >, textContent: < name > })
   * @returns {string | null} The value representation if present, otherwise null
   */
  _nameToValue (name, options) {
    const option = options.find(option => this._equals(option.textContent, name))

    return option ? option.value : null
  }

  /**
   * Check if two strings are equal (case-insensitive).
   *
   * @param {string} str1 - The first string
   * @param {string} str2 - The second string
   * @returns {boolean} Whether or not the strings are equal
   */
  _equals (str1, str2) {
    return str1.toLowerCase() === str2.toLowerCase()
  }
}

export default MovieCoordinator
