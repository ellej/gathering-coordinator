import Day from '../models/day.js'
import HTTPService from '../services/http.js'
import LinkScraper from '../scrapers/link.js'
import TableScraper from '../scrapers/table.js'

/**
 * Coordinator based on calendar availability.
 */
class CalendarCoordinator {
  /**
   * Create a Calendar Coordinator.
   *
   * @param {string} url - The calendar URL starting point
   */
  constructor (url) {
    this._url = url
  }

  /**
   * Coordinate available days.
   *
   * @returns {Day[]} List of days everyone is available
   */
  async run () {
    const calendarHtml = await new HTTPService().getAsText(this._url)
    const calendarLinks = new LinkScraper(calendarHtml).get(this._url)
    const calendars = await this._getCalendars(calendarLinks)

    return this._getAvailableDays(calendars)
  }

  /**
   * Get the days everyone is available.
   *
   * @param {object[]} calendars - List of objects containing days and their availability
   * @returns {Day[]} List of days everyone is available
   */
  _getAvailableDays (calendars) {
    const dayAvailabilityCount = this._getDayAvailabilityCount(calendars)
    const numPeople = calendars.length
    const availableDays = []
    for (const day in dayAvailabilityCount) {
      if (this._everyoneIsAvailable(dayAvailabilityCount[day], numPeople)) {
        availableDays.push(new Day(day))
      }
    }

    return availableDays
  }

  /**
   * Get the number of people available on each day.
   *
   * @param {object[]} calendars - List of objects containing days and their availability
   * @returns {object} Object with the number of people available on each day ({ < day >: < numPeopleAvailable > })
   */
  _getDayAvailabilityCount (calendars) {
    const dayAvailabilityCount = {}
    for (const calendar of calendars) {
      for (let day in calendar) {
        const availability = calendar[day]
        day = day.toLowerCase()
        if (!dayAvailabilityCount[day]) {
          dayAvailabilityCount[day] = 0
        }
        if (this._isAvailable(availability)) {
          dayAvailabilityCount[day]++
        }
      }
    }

    return dayAvailabilityCount
  }

  /**
   * Get the calendars with their days and availability.
   *
   * @param {string[]} urls - The URLs of all calendars to get
   * @returns {object[]} The calendars with days as the keys and availability as the values
   */
  async _getCalendars (urls) {
    const http = new HTTPService()
    const calendars = []
    for (const url of urls) {
      const html = await http.getAsText(url)
      calendars.push(new TableScraper(html).get())
    }

    return calendars
  }

  /**
   * Check if someone is available.
   *
   * @param {string} availability - The availability value
   * @returns {boolean} Whether or not the person is available
   */
  _isAvailable (availability) {
    const regex = /ok/i

    return regex.test(availability)
  }

  /**
   * Check if everyone is available.
   *
   * @param {number} numPeopleAvailable - The number of people available
   * @param {number} numPeopleTotal - The total number of people
   * @returns {boolean} Whether or not everyone is available
   */
  _everyoneIsAvailable (numPeopleAvailable, numPeopleTotal) {
    return numPeopleAvailable === numPeopleTotal
  }
}

export default CalendarCoordinator
