import CalendarCoordinator from './calendar.js'
import Day from '../models/day.js'
import DiningCoordinator from './dining.js'
import Gathering from '../models/gathering.js'
import HTTPService from '../services/http.js'
import LinkScraper from '../scrapers/link.js'
import MovieCoordinator from './movie.js'
import MovieShowing from '../models/movieShowing.js'
import Reservation from '../models/reservation.js'
import UI from '../ui/ui.js'
import { search } from '../utils/strings.js'

/**
 * Coordinator for gathering of friends.
 */
class MainCoordinator {
  /**
   * Create a Main Coordinator.
   *
   * @param {string} originUrl - The main URL starting point
   */
  constructor (originUrl) {
    this._originUrl = originUrl
  }

  /**
   * Coordinate gatherings.
   *
   * @param {boolean} shouldReserveTable - Whether or not to reserve a table
   * @returns {Gathering[]} The gathering suggestions
   */
  async run (shouldReserveTable) {
    const ui = new UI()

    const links = await this._getLinks()
    ui.show('Scraping links...OK')

    const availableDays = await this._getAvailableDays(links)
    ui.show('Scraping available days...OK')

    const availableShowings = await this._getAvailableShowings(links, availableDays)
    ui.show('Scraping showtimes...OK')

    const availableReservations = await this._getAvailableReservations(links, availableShowings)
    ui.show('Scraping possible reservations...OK')

    const suggestions = this._getSuggestions(availableShowings, availableReservations)
    if (shouldReserveTable) {
      this._reserveTable(links, suggestions)
    }

    return suggestions
  }

  /**
   * Get the links to the gathering events.
   *
   * @returns {string[]} links - The links to the gathering events
   */
  async _getLinks () {
    const originHtml = await new HTTPService().getAsText(this._originUrl)

    return new LinkScraper(originHtml).get(this._originUrl)
  }

  /**
   * Get the days everyone is available.
   *
   * @param {string[]} links - The links to the gathering events
   * @returns {Day[]} List of available days
   */
  _getAvailableDays (links) {
    const calendarUrl = search(links, 'calendar')

    return new CalendarCoordinator(calendarUrl).run()
  }

  /**
   * Get the showings available on specific days.
   *
   * @param {string[]} links - The links to the gathering events
   * @param {Day[]} days - The days to get showings for
   * @returns {MovieShowing[]} List of available showings
   */
  _getAvailableShowings (links, days) {
    const cinemaUrl = search(links, 'cinema')

    return new MovieCoordinator(cinemaUrl).run(days)
  }

  /**
   * Get the reservations available based on showings.
   *
   * @param {string[]} links - The links to the gathering events
   * @param {MovieShowing[]} showings - The showings
   * @returns {Reservation[]} List of available reservations
   */
  _getAvailableReservations (links, showings) {
    const diningUrl = search(links, 'dinner')

    return new DiningCoordinator(diningUrl).run(this._getReservationCriteria(showings))
  }

  /**
   * Get the gathering suggestions based on available showings and reservations.
   *
   * @param {MovieShowing[]} showings - The available showings
   * @param {Reservation[]} reservations - The available reservations
   * @returns {Gathering[]} The gathering suggestions
   */
  _getSuggestions (showings, reservations) {
    const suggestions = []
    for (const reservation of reservations) {
      for (const showing of showings) {
        if (this._isPossibleCombination(showing, reservation)) {
          suggestions.push(new Gathering(showing, reservation))
        }
      }
    }

    return suggestions
  }

  /**
   * Check if a showing and reservation can be combined.
   *
   * @param {MovieShowing} showing - The showing
   * @param {Reservation} reservation - The reservation
   * @returns {boolean} Whether or not the combination is possible
   */
  _isPossibleCombination (showing, reservation) {
    if (!showing.day.equals(reservation.day)) {
      return false
    }

    const HOURS_AFTER_MOVIE_TO_EAT = 2
    const time1 = showing.startTime.getFutureTime(HOURS_AFTER_MOVIE_TO_EAT)
    const time2 = reservation.startTime

    return time1.equals(time2)
  }

  /**
   * Get the criteria to base reservations on.
   *
   * @param {MovieShowing[]} showings - The showings
   * @returns {object[]} The criteria to base reservations on
   */
  _getReservationCriteria (showings) {
    const HOURS_AFTER_MOVIE_TO_EAT = 2

    return showings.map(showing => ({
      day: showing.day,
      startTime: showing.startTime.getFutureTime(HOURS_AFTER_MOVIE_TO_EAT)
    }))
  }

  /**
   * Reserve a table.
   *
   * @param {string[]} links - The links to the gathering events
   * @param {Gathering[]} options - The possible options
   */
  _reserveTable (links, options) {
    if (!options.length) {
      return
    }

    const diningUrl = search(links, 'dinner')
    new DiningCoordinator(diningUrl).makeReservation(options[0].reservation)
  }
}

export default MainCoordinator
