import fetch from 'node-fetch'
import { URLSearchParams } from 'url'

/**
 * Service for making HTTP requests.
 */
class HTTPService {
  /**
   * Make a GET request for a resource.
   *
   * @param {string} url - The URL to send request to
   * @param {object} options - Options for the HTTP request
   * @returns {Promise<Response>} The HTTP response
   */
  get (url, options = {}) {
    return fetch(url, { ...options, method: 'GET' })
  }

  /**
   * Make a GET request for a resource and get the response as plain text or HTML.
   *
   * @param {string} url - The URL to send request to
   * @param {object} options - Options for the HTTP request
   * @returns {Promise<string>} The HTTP response body as plain text or HTML
   */
  async getAsText (url, options = {}) {
    const res = await this.get(url, options)

    return res.text()
  }

  /**
   * Make a GET request for a resource and parse the response as JSON.
   *
   * @param {string} url - The URL to send request to
   * @param {object} options - Options for the HTTP request
   * @returns {Promise<any>} The HTTP response body parsed as JSON
   */
  async getAsJson (url, options = {}) {
    const res = await this.get(url, options)

    return res.json()
  }

  /**
   * Make a POST request to an endpoint.
   *
   * @param {string} url - The URL to send request to
   * @param {object} data - The data to send
   * @param {object} options - Options for the HTTP request
   * @returns {Promise<Response>} The HTTP response
   */
  post (url, data, options = {}) {
    return fetch(url, {
      ...options,
      method: 'POST',
      body: this._buildParams(data)
    })
  }

  /**
   * Get params to send with the HTTP request.
   *
   * @param {object} data - The param data
   * @returns {URLSearchParams} The params
   */
  _buildParams (data) {
    const params = new URLSearchParams()
    for (const property in data) {
      params.append(property, data[property])
    }

    return params
  }
}

export default HTTPService
