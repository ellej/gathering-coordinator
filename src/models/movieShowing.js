import Day from './day.js'
import Time from './time.js'

/**
 * Representing a showing of a movie.
 */
class MovieShowing {
  /**
   * Create a Movie Showing.
   *
   * @param {string} name - The name of the movie
   * @param {Day} day - The day the showing is on
   * @param {Time} startTime - The time the showing starts
   */
  constructor (name, day, startTime) {
    this.setName(name)
    this._day = day
    this._startTime = startTime
  }

  /**
   * Returns the name of the movie.
   *
   * @returns {string} - The name of the movie
   */
  get name () {
    return this._name
  }

  /**
   * Set the name of the movie.
   *
   * @param {string} name - The name of the movie
   */
  setName (name) {
    if (!name.length) {
      throw new Error('Name of the movie must be at least 1 character long.')
    }

    this._name = name
  }

  /**
   * Returns the day of the showing.
   *
   * @returns {Day} - The day of the showing
   */
  get day () {
    return this._day
  }

  /**
   * Returns the time the showing starts.
   *
   * @returns {Time} - The time the showing starts
   */
  get startTime () {
    return this._startTime
  }
}

export default MovieShowing
