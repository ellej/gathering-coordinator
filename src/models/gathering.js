import Day from './day.js'
import MovieShowing from './movieShowing.js'
import Reservation from './reservation.js'

/**
 * Representing a gathering for going to the movies and dining out.
 */
class Gathering {
  /**
   * Create a Gathering.
   *
   * @param {MovieShowing} movie - The movie and specific showing
   * @param {Reservation} reservation - The reservation
   */
  constructor (movie, reservation) {
    this._day = movie.day
    this._movie = movie
    this._reservation = reservation
  }

  /**
   * Returns the day of the gathering.
   *
   * @returns {Day} - The day of the gathering
   */
  get day () {
    return this._day
  }

  /**
   * Returns the movie/showing.
   *
   * @returns {MovieShowing} - The movie/showing
   */
  get movie () {
    return this._movie
  }

  /**
   * Returns the reservation.
   *
   * @returns {Reservation} - The reservation
   */
  get reservation () {
    return this._reservation
  }
}

export default Gathering
