/**
 * Representing a day of the week.
 */
class Day {
  /**
   * Create a Day.
   *
   * @param {string} name - The name of the day
   */
  constructor (name) {
    this.setName(name)
  }

  /**
   * Returns the name of the day.
   *
   * @returns {string} - The name of the day
   */
  get name () {
    return this._name
  }

  /**
   * Set the name.
   *
   * @param {string} name - The name of the day
   */
  setName (name) {
    if (!name.length) {
      throw new Error('Please provide a name for the day.')
    }

    const DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    const validName = DAYS.find(day => day.toLowerCase() === name.toLowerCase())
    if (!validName) {
      throw new Error(`Invalid name: ${name}.`)
    }

    this._name = validName
  }

  /**
   * Check if the day is the same is another day.
   *
   * @param {Day} day - The day to compare to
   * @returns {boolean} Whether or not the days are the same
   */
  equals (day) {
    return day.name === this._name
  }
}

export default Day
