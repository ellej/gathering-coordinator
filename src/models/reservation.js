import Day from './day.js'
import Time from './time.js'

/**
 * Representing a reservation.
 */
class Reservation {
  /**
   * Create a Reservation.
   *
   * @param {Day} day - The day the reservation is on
   * @param {Time} startTime - The time the reservation starts
   * @param {Time} endTime - The time the reservation ends
   * @param {string} idName - The name of the reservation identifier
   * @param {string} idValue - The value of the reservation identifier
   */
  constructor (day, startTime, endTime, idName, idValue) {
    this._day = day
    this._startTime = startTime
    this._endTime = endTime
    this._idName = idName
    this._idValue = idValue
  }

  /**
   * Returns the day of the reservation.
   *
   * @returns {Day} - The day of the reservation
   */
  get day () {
    return this._day
  }

  /**
   * Returns the time the reservation starts.
   *
   * @returns {Time} - The time the reservation starts
   */
  get startTime () {
    return this._startTime
  }

  /**
   * Returns the time the reservation ends.
   *
   * @returns {Time} - The time the reservation ends
   */
  get endTime () {
    return this._endTime
  }

  /**
   * Returns the name of the reservation identifier.
   *
   * @returns {string} - The name of the reservation identifier
   */
  get idName () {
    return this._idName
  }

  /**
   * Returns the value of the reservation identifier.
   *
   * @returns {string} - The value of the reservation identifier
   */
  get idValue () {
    return this._idValue
  }
}

export default Reservation
