/**
 * Representing a time in 24-hour format.
 */
class Time {
  /**
   * Create a Time.
   *
   * @param {string} time - The time in 24-hour format (< hour > or < hour >:< minute >)
   */
  constructor (time) {
    this.setTime(time)
  }

  /**
   * Set the time.
   *
   * @param {string} time - The time in 24-hour format (< hour > or < hour >:< minute >)
   */
  setTime (time) {
    if (time.length < 2) {
      throw new Error(`Invalid time format. Expected <hour> or <hour>:<minute> in 24-hour format. Received: ${time}`)
    }

    time = this._parseTime(time)
    this._hour = time.hour
    this._minute = time.minute
  }

  /**
   * Returns the hour of the day in 24-hour format.
   *
   * @returns {string} - The hour of the day
   */
  get hour () {
    return this._hour
  }

  /**
   * Returns the minute of the hour.
   *
   * @returns {string} - The minute of the hour
   */
  get minute () {
    return this._minute
  }

  /**
   * Get the time a certain number of hours ahead.
   *
   * @param {number} hours - The number of hours ahead of the time to get.
   * @returns {Time} The new time
   */
  getFutureTime (hours) {
    if (!Number.isInteger(hours) || hours < 1 || hours > 23) {
      return this
    }

    let newHour = Number.parseInt(this._hour) + hours
    if (newHour > 24) {
      newHour -= 24
    }

    return new Time(newHour.toString() + ':' + this._minute)
  }

  /**
   * Check if the time is the same is another time.
   *
   * @param {Time} time - The time to compare to
   * @returns {boolean} Whether or not the times are the same
   */
  equals (time) {
    return time.hour === this._hour && time.minute === this._minute
  }

  /**
   * Get the parsed time.
   *
   * @param {string} time - The time (< hour > or < hour >:< minute >)
   * @returns {object} Time with hour and minute property
   */
  _parseTime (time) {
    return {
      hour: time.split(':')[0].padStart(2, '0'),
      minute: time.split(':')[1] || '00'
    }
  }

  /**
   * Get the stringified formatted time in 24-hour format.
   *
   * @returns {string} The time formatted
   */
  format () {
    return this._hour + ':' + this._minute
  }
}

export default Time
