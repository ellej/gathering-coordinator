import Gathering from '../models/gathering.js'

/**
 * UI for communicating with the user.
 */
class UI {
  /**
   * Show a message to the user.
   *
   * @param {string} message - The message to show to the user
   */
  show (message) {
    console.log(message)
  }

  /**
   * Show the suggestions of gatherings to the user.
   *
   * @param {Gathering[]} suggestions - The gathering suggestions
   */
  showSuggestions (suggestions) {
    console.log('\nSuggestions')
    console.log('===========')
    suggestions.forEach(suggestion => console.log(this._stringify(suggestion)))
  }

  /**
   * Get the gathering stringified.
   *
   * @param {Gathering} gathering - The gathering
   * @returns {string} The stringified gathering
   */
  _stringify (gathering) {
    return `* On ${gathering.day.name}, "${gathering.movie.name}" begins at ${gathering.movie.startTime.format()}, and there is a free table to book between ${gathering.reservation.startTime.format()}-${gathering.reservation.endTime.format()}.`
  }

  /**
   * Show an error message to the user.
   *
   * @param {string} message - The error message to show to the user
   */
  error (message) {
    console.log('------------------------------')
    console.error('ERROR: ', message)
    console.log('------------------------------')
  }
}

export default UI
