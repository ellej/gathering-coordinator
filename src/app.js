import MainCoordinator from './coordinators/main.js'
import UI from './ui/ui.js'
import { validateUrl } from './utils/urls.js'

/**
 * Main function of application.
 */
const main = async () => {
  const ui = new UI()

  try {
    // Parse third and fourth arguments
    let [,, entryUrl, shouldReserveTable] = process.argv
    const validEntryUrl = validateUrl(entryUrl)
    if (!validEntryUrl) {
      throw new Error(`Invalid URL: ${entryUrl}`)
    }

    const suggestions = await new MainCoordinator(validEntryUrl).run(shouldReserveTable === 'yes')
    ui.showSuggestions(suggestions)
  } catch (err) {
    ui.error(err.message)
  }
}

main()
