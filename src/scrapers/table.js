import Scraper from './scraper.js'

/**
 * Web scraper for tables.
 */
class TableScraper extends Scraper {
  /**
   * Get the first table in the HTML body.
   *
   * @returns {object} Table with the headers as the keys and cells as the values
   */
  get () {
    const { window: { document } } = this.getParsedHtml()

    return this._parseTable(document.querySelector('table'))
  }

  /**
   * Get a table with header values and corresponding cell values.
   *
   * @param {HTMLElement} tableElem - The HTML table element
   * @returns {object} Table with headers as the keys and cells as the values
   */
  _parseTable (tableElem) {
    const headerValues = this._getHeaderValues(tableElem)
    const cellValues = this._getCellValues(tableElem)
    if (headerValues.length !== cellValues.length) {
      throw new Error(`Expected ${headerValues.length} cell values, found ${cellValues.length}.`)
    }

    // Place the table header value with its cell value onto an object ({ <headerValue>: <cellValue> })
    const table = {}
    for (let i = 0; i < headerValues.length; i++) {
      table[headerValues[i]] = cellValues[i]
    }

    return table
  }

  /**
   * Get the header values from an HTML table.
   *
   * @param {HTMLElement} tableElem - The HTML table element
   * @returns {string[]} List of the header values
   */
  _getHeaderValues (tableElem) {
    return this._getTextContents(tableElem, 'th')
  }

  /**
   * Get the cell values from an HTML table.
   *
   * @param {HTMLElement} tableElem - The HTML table element
   * @returns {string[]} List of the cell values
   */
  _getCellValues (tableElem) {
    return this._getTextContents(tableElem, 'td')
  }

  /**
   * Get the text contents from HTML elements of a parent.
   *
   * @param {HTMLElement} parentElem - The HTML parent element
   * @param {string} tag - The tag of the child elements to get the text content from
   * @returns {string[]} List of the text contents
   */
  _getTextContents (parentElem, tag) {
    return Array.from(parentElem.querySelectorAll(tag)).map(elem => elem.textContent)
  }
}

export default TableScraper
