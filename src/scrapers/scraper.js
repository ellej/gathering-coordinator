import { JSDOM } from 'jsdom'

/**
 * A web scraper.
 */
class Scraper {
  /**
   * Create a Scraper.
   *
   * @param {string} body - The plain text HTML body
   */
  constructor (body) {
    this._plainTextHtml = body
    this._parsedHtml = this._parseHtml(body)
  }

  /**
   * Get the plain text HTML.
   *
   * @returns {string} The plain text HTML body
   */
  getPlainTextHtml () {
    return this._plainTextHtml
  }

  /**
   * Get the parsed HTML body.
   *
   * @returns {JSDOM} The HTML DOM
   */
  getParsedHtml () {
    return this._parsedHtml
  }

  /**
   * Parse the plain text HTML body.
   *
   * @param {string} body - The plain text HTML body
   * @returns {JSDOM} The HTML DOM
   */
  _parseHtml (body) {
    return new JSDOM(body)
  }
}

export default Scraper
