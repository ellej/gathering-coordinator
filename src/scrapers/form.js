import Scraper from './scraper.js'

/**
 * Web scraper for forms.
 */
class FormScraper extends Scraper {
  /**
   * Get the options of a select element.
   *
   * @param {string} selectId - The id of the select element to get options from
   * @returns {object[]} The options containing their values and corresponding text contents
   */
  getOptions (selectId) {
    const { window: { document } } = this.getParsedHtml()

    const options = []
    document.querySelectorAll(`select#${selectId} option`).forEach(elem => {
      const option = this._getOption(elem)
      if (option) {
        options.push(option)
      }
    })

    return options
  }

  /**
   * Get the inputs.
   *
   * @param {string} type - The type of the input
   * @returns {object[]} List of the inputs with name and value
   */
  getInputs (type) {
    const { window: { document } } = this.getParsedHtml()

    return Array.from(document.querySelectorAll(`input[type=${type}]`)).map(elem => ({
      name: elem.getAttribute('name'),
      value: elem.getAttribute('value')
    }))
  }

  /**
   * Get a formatted option.
   *
   * @param {HTMLElement} optionElem - The HTML option element
   * @returns {object | null} Option containing its value and corresponding text content if not disabled, otherwise null
   */
  _getOption (optionElem) {
    if (optionElem.hasAttribute('disabled')) {
      return null
    }

    return {
      value: optionElem.getAttribute('value'),
      textContent: optionElem.textContent
    }
  }
}

export default FormScraper
