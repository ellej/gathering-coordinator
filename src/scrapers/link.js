import Scraper from './scraper.js'
import { removeLeadingChar, removeTrailingChar } from '../utils/strings.js'
import { validateUrl } from '../utils/urls.js'

/**
 * Web scraper for links.
 */
class LinkScraper extends Scraper {
  /**
   * Get the unique links.
   *
   * @param {string} baseUrl - The base URL
   * @returns {string[]} The unique links
   */
  get (baseUrl = '') {
    const { window: { document } } = this.getParsedHtml()

    const links = new Set()
    document.querySelectorAll('a').forEach(elem => {
      let link = this._getFullLink(baseUrl, elem.getAttribute('href'))
      link = validateUrl(link)
      if (link) {
        links.add(link)
      }
    })

    return [...links]
  }

  /**
   * Get the full link of an href value.
   *
   * @param {string} baseUrl - The base URL
   * @param {string} href - The href value of an HTML element
   * @returns {string} The full link
   */
  _getFullLink (baseUrl, href) {
    if (this._isRelativeUrl(href)) {
      href = this._relativeToAbsoluteUrl(baseUrl, href)
    }

    return href
  }

  /**
   * Check if a URL is relative.
   *
   * @param {string} url - The URL to test
   * @returns {boolean} Whether or not the URL is relative
   */
  _isRelativeUrl (url) {
    // Check if url starts with ./ or /
    const regex = /^.?\//

    return regex.test(url)
  }

  /**
   * Convert a relative URL into an absolute one.
   *
   * @param {string} baseUrl - The base URL
   * @param {string} relativeUrl - The relative URL
   * @returns {string} The absolute URL
   */
  _relativeToAbsoluteUrl (baseUrl, relativeUrl) {
    baseUrl = removeTrailingChar(baseUrl, '/')
    relativeUrl = removeLeadingChar(relativeUrl, '.')

    return baseUrl + relativeUrl
  }
}

export default LinkScraper
