# A Coordinator for Gatherings of Friends Using Web Scraping

A Node.js command line program that coordinates a day for friends to gather, see a movie, and reserve a table to go out to dinner after the movie, based on availabilities.

## Table of Contents

- [Basis of the Coordination](#basis-of-the-coordination)
- [Basis of the HTML Scraping](#basis-of-the-html-scraping)
  - [Entry point](#entry-point)
  - [Calendars](#calendars)
  - [Cinema](#cinema)
  - [Restaurant](#restaurant)
- [Program Flow](#program-flow)
- [Program Output](#program-output)
- [Getting Started](#getting-started)
  - [Set up development environment](#set-up-development-environment)
  - [Clone the repo](#clone-the-repo)
  - [Install dependencies](#install-dependencies)
  - [Run the program](#run-the-program)
- [Notes](#notes)
  - [URLs](#urls)

## Basis of the Coordination

The suggestions and coordination by the program is based on:
* The availability of each person.
  * Scraped from their shared online calendar (HTML).
* The available movies on any given day and time.
  * Scraped from the website of the fictitious movie theater (HTML).
* The available reservation slots at their favorite restaurant.
  * Scraped from the website of the fictitious restaurant (HTML).

## Basis of the HTML Scraping

By scrutinizing the HTML of the sites, the web scraper scrapes the HTML based on:

#### Entry point

* An entry point URL
  * Should contain a list with 3 items:
    * A link to where every person's calendar is.
    * A link to the cinema / movie theater.
    * A link to the restaurant.
* Example:
  ```shell
  1. Calendars
  2. The cinema!
  3. Zeke's Bar!
  ```

#### Calendars

* A calendars URL
  * Should contain a list with items (one item per person):
    * A link to the person's calendar.
* Individual calendar URLs
  * Should contain a table with entries for 0 or all days of the week:
    * Each entry states whether or not the person is available.
* Example:
  ```shell
  Friday      Saturday      Sunday

  ok          OK            -
  ```

#### Cinema

* A cinema URL
  * Should contain a form for selecting a day and a movie.
    * Submitting the form via an HTTP request returns an array of objects with the time for that movie at that day and whether or not there are seats left.
* Example:
  ```shell
  Friday

  The Dark Knight Rises

  16:00 Seats available
  18:00 Seats available
  21:00 Fully booked
  ```

#### Restaurant

* A restaurant URL
  * Should contain a form for logging in.
    * Submitting the form returns the reservation URL in the response header to redirect to.
* A reservation URL
  * Should contain a form showing the reservation times and whether or not a table is free.
    * Submitting the form creates a reservation.
* Example:
  ```shell
  Friday

  14-16 Free
  16-18 Free
  18-20 Fully booked
  20-22 Free
  ```

## Program Flow

The flow of the program logic is as follows:

1. The user starts the program and passes the entry URL as an argument (`npm start <entry url>`), and optionally `yes` to make the first available dining table reservation (`npm start <entry url> yes`).
2. The program determines which days of the week everyone is available.
3. The program determines what movie showings are available those days (there may be multiple showings for each movie).
4. The program determines what dinner reservations can be made after the available movie showings.
5. If `yes` was passed at Step 1, the program also makes a reservation.
6. The program outputs the possible gathering suggestions.

## Program Output

If there are possible suggestions for everyone to gather, the program will output those in the following form:

```shell
Scraping links...OK
Scraping available days...OK
Scraping showtimes...OK
Scraping possible reservations...OK

Suggestions
===========
* On Friday, "The Dark Knight" begins at 16:00, and there is a free table to book between 18:00-20:00.
* On Friday, "Titanic" begins at 16:00, and there is a free table to book between 18:00-20:00.
```

## Getting Started

### Set up development environment

This program requires that you have [Node.js](https://nodejs.org/) installed on your machine.

### Clone the repo

```bash
# using https
git clone https://gitlab.com/ellej/gathering-coordinator.git

# using ssh
git clone git@gitlab.com:ellej/gathering-coordinator.git
```

### Install dependencies

```bash
cd gathering-coordinator
npm install
```

### Run the program

There are 2 entry URLs to choose from:
* https://cscloud6-127.lnu.se/scraper-site-1
* https://cscloud6-127.lnu.se/scraper-site-2

Both provide the same information but have slightly different HTML in order to generalize the web scrapers a bit more.

From the root directory of the project, run:

```bash
# Without making a dining reservation
npm start <entry url 1 or 2>

# With making a dining reservation
npm start <entry url 1 or 2> yes
```

## Notes

### URLs
* The URLs to the calendars, movie theater, and restaurant, are maintained and provided by Linnaeus University.
